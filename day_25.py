

def distance(a, b):
    return sum(abs(x-y) for x, y in zip(a, b))


def can_join(point, constellation):
    for p in constellation:
        if distance(point, p) <= 3:
            return True
    return False


def main():
    constellations = list()
    points = []
    for line in open('day_25.data'):
        x, y, z, w = (int(d) for d in line.split(','))
        points.append((x, y, z, w))

    constellation = {points[0]}
    points.remove(points[0])
    while len(points) > 0:
        merged_a_point = False
        idx = 0
        while idx < len(points):
            point = points[idx]
            if can_join(point, constellation):
                constellation.add(point)
                points.remove(point)
                merged_a_point = True
            else:
                idx += 1
        if not merged_a_point:
            constellations.append(constellation)
            constellation = {points[0]}
            points.remove(points[0])
    print(f'Number of constellations: {len(constellations)}')


if __name__ == '__main__':
    main()
