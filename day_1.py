
result = 0
all = set()
dup = False

for r in range(1000):
    with open('day_1.data', 'r') as f:
        for line in f:
            result += int(line)
            if result in all:
                print('first dup:', result)
                dup = True
                break
            all.add(result)

    if r == 0:
        print('sum:', result)

    if dup:
        break

