

def load_data():
    all = set()
    req = dict()
    with open('day_7.data', 'r') as file:
        for line in file:
            words = line.split(' ')
            w1, w2 = (words[1], words[7])
            all.add(w1)
            all.add(w2)
            if w2 not in req:
                req[w2] = list()
            req[w2].append(w1)
    return (all, req)


def remove_req(req, w):
    to_remove = set()
    for key, r in req.items():
        if w in r:
            r.remove(w)
            if len(r) == 0:
                to_remove.add(key)
    for key in to_remove:
        del req[key]


all, req = load_data()
step_order = ''

while len(all) > 0:
    for w in sorted(all):
        if w not in req:
            step_order += w
            all.remove(w)
            remove_req(req, w)
            break
print('part1', step_order)

all, req = load_data()
time_taken = 0
working = list()
free_workers = 4
step_order = ''

while len(all) > 0 or len(working) > 0:
    for w in sorted(all):
        if free_workers == 0:
            break
        if w not in req:
            all.remove(w)
            working.append([w, 61 + int(w.encode()[0]) - int('A'.encode()[0])])
            free_workers -= 1
    print(working, time_taken)
    worker_finished = False
    while not worker_finished:
        for w in sorted(working): # hack 101, use sorted to get a copy so that the working.remove can remove while looping
            w[1] -= 1
            if w[1] == 0:
                remove_req(req, w[0])
                free_workers += 1
                step_order += w[0]
                working.remove(w)
                worker_finished = True
        time_taken += 1

print('part2', time_taken, step_order)
