
from matplotlib import pyplot, animation


class World2d:
    def __init__(self, iter_2d, ignore_squares=[]):
        self.squares = None
        self.default_square = ' '

        self.fill_squares(iter_2d, ignore_squares)

        self.plot_ax = None
        self.plot_objects = None
        self.plot_inverted_y = None
        self.plot_control_y_limits = None
        self.plot_y_limits = None

    def fill_squares(self, iter_2d, ignore_squares):
        self.squares = dict()
        for y, row in enumerate(iter_2d):
            for x, square in enumerate(row):
                if square not in ignore_squares:
                    self.squares[(x, y)] = square

    def adj_ortho_squares(self, x, y):
        for dx, dy in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
            yield self.squares.get((x + dx, y + dy), self.default_square)

    def adj_squares(self, x, y):
        for dx, dy in [(1, 0), (0, 1), (-1, 0), (0, -1), (1, 1), (-1, 1), (-1, -1), (1, -1)]:
            yield self.squares.get((x + dx, y + dy), self.default_square)

    @property
    def xy_limits(self):
        min_x = min(x for x, y in self.squares)
        max_x = max(x for x, y in self.squares)
        min_y = min(y for x, y in self.squares)
        max_y = max(y for x, y in self.squares)
        return min_x, max_x, min_y, max_y

    @property
    def coord_within_limits(self):
        min_x, max_x, min_y, max_y = self.xy_limits
        for y in range(min_y, max_y + 1):
            for x in range(min_x, max_x + 1):
                yield x, y

    def get_coord_of(self, goal_square):
        x, y = list(), list()
        for coord, square in self.squares.items():
            if square == goal_square:
                tx, ty = coord
                x.append(tx)
                y.append(ty)
        return x, y

    def get_string(self):
        world_string = ''
        for x, y in self.coord_within_limits:
            if x == 0:
                world_string += '\n'
            world_string += self.squares.get((x, y), self.default_square)
        return world_string

    def print(self):
        print(self.get_string())

    def animate(self, minute):
        for key, obj in self.plot_objects.items():
            obj.set_data(self.get_coord_of(key))

        min_x, max_x, min_y, max_y = self.xy_limits
        if not self.plot_control_y_limits:
            min_y, max_y = self.plot_y_limits
        self.plot_ax.set_xlim(min_x - 1, max_x + 1)
        if self.plot_inverted_y:
            self.plot_ax.set_ylim(max_y + 1, min_y - 1)
        else:
            self.plot_ax.set_ylim(min_y - 1, max_y + 1)
        return self.plot_objects.values()

    def run_animation(self, title, plot_objects, interval=100, inverted_y=False, control_y_limits=True):
        self.plot_inverted_y = inverted_y
        self.plot_control_y_limits = control_y_limits
        fig = pyplot.figure(title)
        self.plot_ax = pyplot.axes()

        self.plot_objects = dict()
        for key in plot_objects:
            self.plot_objects[key], = self.plot_ax.plot([], [], plot_objects[key])

        anim = animation.FuncAnimation(fig, self.animate, interval=interval, blit=True, repeat=False)
        pyplot.show()
