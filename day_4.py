
import collections
import datetime


DataPointRaw = collections.namedtuple('DataPointRaw', ['year', 'month', 'day', 'hour', 'minute', 'data'])
DataPoint = collections.namedtuple('DataPoint', ['ts', 'data'])


def parse_data(s):
    s = s.replace('[', '').replace(']', '').replace('\n', '')
    for p in ['-', ':']:
        s = s.replace(p, ' ')
    raw = DataPointRaw(*(s.split(' ', 5)))
    dp = DataPoint(datetime.datetime(int(raw.year),
                                     int(raw.month),
                                     int(raw.day),
                                     int(raw.hour),
                                     int(raw.minute),
                                     ), raw.data)
    return dp


def find_max(fn, data):
    max_fn = 0
    max_minute = 0
    max_id = 0
    for id, sm in data.items():
        if fn(sm) > max_fn:
            max_fn = fn(sm)
            max_minute = 0
            for i in range(60):
                if sm[i] > sm[max_minute]:
                    max_minute = i
            max_id = id
    return (max_id, max_minute)


data_points = dict()
sleep = dict()

with open('day_4.data', 'r') as file:
    for line in file:
        dp = parse_data(line)
        data_points[dp.ts] = dp

guard_id = None
falls = None
for ts in sorted(data_points):
    dp = data_points[ts]
    for p in dp.data.split(' '):
        if p[0] == '#':
            guard_id = int(p[1:])
    if dp.data == 'falls asleep':
        falls = ts.minute
    elif dp.data == 'wakes up':
        wakes = ts.minute
        if guard_id not in sleep:
            sleep[guard_id] = [0] * 60
        for m in range(falls, wakes):
            sleep[guard_id][m] += 1

max_id, max_minute = find_max(sum, sleep)
print('part1', max_id * max_minute)

max_id, max_minute = find_max(max, sleep)
print('part2', max_id * max_minute)
