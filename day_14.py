

def gen_new_recipes(rec, elfs):
    n = rec[elfs[0]] + rec[elfs[1]]
    if n >= 10:
        rec.append(n // 10)
    rec.append(n % 10)


def move_elf(rec, elfs):
    for i in range(len(elfs)):
        elfs[i] = (elfs[i] + 1 + rec[elfs[i]]) % len(rec)


def is_match(rec, pattern, offset):
    for i, p in enumerate(pattern):
        if rec[offset + i] != p:
            return False
    return True


def find_score(rec, elfs, n, pattern):
    pattern_next_check = 0
    pattern_pos = None
    while len(rec) < 10 + n or pattern_pos is None:
        gen_new_recipes(rec, elfs)
        move_elf(rec, elfs)
        if pattern_pos is None:
            while pattern_next_check + len(pattern) <= len(rec):
                if is_match(rec, pattern, pattern_next_check):
                    pattern_pos = pattern_next_check
                pattern_next_check += 1
    return ''.join(str(x) for x in rec[n:n + 10]), pattern_pos


print(find_score([3, 7], [0, 1], 9, [5, 1, 5, 8, 9]))
print(find_score([3, 7], [0, 1], 5, [0, 1, 2, 4, 5]))
print(find_score([3, 7], [0, 1], 18, [9, 2, 5, 1, 0]))
print(find_score([3, 7], [0, 1], 2018, [5, 9, 4, 1, 4]))
print(find_score([3, 7], [0, 1], 909441, [9, 0, 9, 4, 4, 1]))





