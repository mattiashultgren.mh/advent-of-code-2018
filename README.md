# Advent of code 2018

Just for fun puzzle solving

## Day 1 to 15
Solved.

## Day 17 to 18
Solved.

## Day 23
Doesn't truly solve part 2, but it finds the correct answer.
The problem is that the way that it searches there is no way to prove
that the best answer found is really the best possible.

## Day 25
Solved.