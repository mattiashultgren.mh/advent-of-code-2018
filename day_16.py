
from time import time

from computer import Computer


def find_matching_opcodes(code, before, after):
    matching = set()
    for opcode, func in Computer.opcodes.items():
        state = list(before)
        state[code[3]] = func(state, code)
        if tuple(state) == after:
            matching.add(opcode)
    return matching


def main():
    before_str = 'Before: '
    after_str = 'After: '
    before, after = None, None
    code = []
    three_or_more = 0
    opcode_number_to_name = []
    for i in range(len(Computer.opcodes)):
        opcode_number_to_name.append(set(Computer.opcodes.keys()))

    for line in open('day_16.data'):
        line = line.replace('\n', '')
        if line.startswith(before_str):
            before = tuple(eval(line[len(before_str):]))
        elif line.startswith(after_str):
            after = tuple(eval(line[len(after_str):]))
            code = code[0]
            matching_opcodes = find_matching_opcodes(code, before, after)
            if len(matching_opcodes) >= 3:
                three_or_more += 1
            opcode_number_to_name[code[0]] &= matching_opcodes
            before, after = None, None
            code = []
        elif len(line) == 0:
            pass
        else:
            code.append(tuple(map(int, line.split(' '))))

    updated = True
    while updated:
        updated = False
        for num, name in enumerate(opcode_number_to_name):
            if len(name) == 1:
                for num2, name2 in enumerate(opcode_number_to_name):
                    if num != num2 and len(name & name2) > 0:
                        name2 -= name
                        updated = True

    print(f'There was {three_or_more} samples that behaved as three or more opcodes')
    for num, name in enumerate(opcode_number_to_name):
        opcode_number_to_name[num] = list(name)[0]

    state = [0] * 4
    for opcode, a, b, c in code:
        instruction = (opcode_number_to_name[opcode], a,b,c)
        Computer.execute(state, instruction)
    print(f'Register 0 after the test program is {state[0]}')


if __name__ == '__main__':
    start = time()
    main()
    print(f'Time: {time()-start:.3}')
