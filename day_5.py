
import string


def react(s):
    old_len = len(s)
    while True:
        old_len = len(s)
        for low in string.ascii_lowercase:
            up = low.upper()
            s = s.replace(up + low, '')
            s = s.replace(low + up, '')
        if old_len == len(s):
            break
    return s


with open('day_5.data', 'r') as file:
    s = file.read().replace('\n', '')

s = react(s)
print('part1', len(s))

lowest_len = None
for low_erase in string.ascii_lowercase:
    s2 = react(s.replace(low_erase, '').replace(low_erase.upper(), ''))
    if (lowest_len is None) or (len(s2) < lowest_len):
        lowest_len = len(s2)

print('part2', lowest_len)
