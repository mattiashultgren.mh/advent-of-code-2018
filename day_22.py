
from time import time
import networkx as nx


class Cave:
    def __init__(self, depth, target):
        self.depth = depth
        self.target = target
        self.geologic_indices = dict()

    def add_pos_in_geologic_indices(self, x, y):
        if x == 0 or y == 0:
            self.geologic_indices[(x, y)] = x*16807 + y*48271
        elif (x, y) == self.target:
            self.geologic_indices[(x, y)] = 0
        else:
            self.geologic_indices[(x, y)] = self.erosion_level(x-1, y) * self.erosion_level(x, y-1)

    def erosion_level(self, x, y):
        pos = (x, y)
        if pos not in self.geologic_indices:
            self.add_pos_in_geologic_indices(x, y)
        return (self.geologic_indices[pos] + self.depth) % 20183

    def region_risk(self, x, y):
        return self.erosion_level(x, y) % 3

    def region_type(self, x, y):
        return self.region_risk(x, y)

    @staticmethod
    def get_square_name(x, y, tool):
        return '(' + str(x) + ',' + str(y) + ',' + tool + ')'

    def get_graph(self, max_x, max_y):
        g = nx.Graph()
        for y in range(max_y+1):
            for x in range(max_x+1):
                square = self.region_type(x, y)
                if square == 0:
                    g.add_edge(self.get_square_name(x, y, 'c'), self.get_square_name(x, y, 't'), weight=7)
                elif square == 1:
                    g.add_edge(self.get_square_name(x, y, 'c'), self.get_square_name(x, y, 'n'), weight=7)
                else:
                    g.add_edge(self.get_square_name(x, y, 't'), self.get_square_name(x, y, 'n'), weight=7)
                for dx, dy in [(1, 0), (0, 1)]:
                    if x >= dx and y >= dy:
                        tx, ty = x-dx, y-dy
                        other = self.region_type(tx, ty)
                        if square == 0 and other == 0:
                            g.add_edge(self.get_square_name(x, y, 'c'), self.get_square_name(tx, ty, 'c'), weight=1)
                            g.add_edge(self.get_square_name(x, y, 't'), self.get_square_name(tx, ty, 't'), weight=1)
                        elif square == 1 and other == 1:
                            g.add_edge(self.get_square_name(x, y, 'c'), self.get_square_name(tx, ty, 'c'), weight=1)
                            g.add_edge(self.get_square_name(x, y, 'n'), self.get_square_name(tx, ty, 'n'), weight=1)
                        elif square == 2 and other == 2:
                            g.add_edge(self.get_square_name(x, y, 't'), self.get_square_name(tx, ty, 't'), weight=1)
                            g.add_edge(self.get_square_name(x, y, 'n'), self.get_square_name(tx, ty, 'n'), weight=1)
                        elif (square == 0 and other == 1) or (square == 1 and other == 0):
                            g.add_edge(self.get_square_name(x, y, 'c'), self.get_square_name(tx, ty, 'c'), weight=1)
                        elif (square == 0 and other == 2) or (square == 2 and other == 0):
                            g.add_edge(self.get_square_name(x, y, 't'), self.get_square_name(tx, ty, 't'), weight=1)
                        elif (square == 1 and other == 2) or (square == 2 and other == 1):
                            g.add_edge(self.get_square_name(x, y, 'n'), self.get_square_name(tx, ty, 'n'), weight=1)
        return g


def main():
    cave = Cave(7305, (13, 734))
    # cave = Cave(510, (10, 10))

    risk = 0
    for y in range(cave.target[1]+1):
        for x in range(cave.target[0]+1):
            risk += cave.region_risk(x, y)
    print(f'Risk: {risk}')

    extra = 0
    while True:
        graph = cave.get_graph(cave.target[0]+extra, cave.target[1]+extra)

        shortest_path_cost = nx.shortest_path_length(graph,
                                                     cave.get_square_name(0, 0, 't'),
                                                     cave.get_square_name(*cave.target, 't'),
                                                     weight='weight')
        if extra == 0:
            extra = (shortest_path_cost - sum(cave.target)) // 2
        else:
            break
    print(f'Shortest path to target is : {shortest_path_cost}')


if __name__ == '__main__':
    start = time()
    main()
    print(f'Time: {time()-start:.3}')
