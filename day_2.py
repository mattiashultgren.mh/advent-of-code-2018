

def calc_checksum(code):
    letters = dict()
    checksum = [0] * 2

    for letter in code:
        if letter in letters:
            letters[letter] += 1
        else:
            letters[letter] = 1

    for k, v in letters.items():
        if v == 2:
            checksum[0] = 1
        elif v == 3:
            checksum[1] = 1
    return tuple(checksum)


def distance(s1, s2):
    d = 0
    for i in range(len(s1)):
        if s1[i] != s2[i]:
            d += 1
    return d


def common(s1, s2):
    com = ''
    for i in range(len(s1)):
        if s1[i] == s2[i]:
            com += s1[i]
    return com


allchk = list()
allid = list()


with open('day_2.data', 'r') as f:
    for line in f:
        line = line.replace('\n', '')
        chk = calc_checksum(line)
        allchk.append(chk)

        for other in allid:
            if distance(other, line) == 1:
                print(common(other, line))
        allid.append(line)

sums = [0] * 2
for t in allchk:
    for i in range(2):
        sums[i] += t[i]

print('box_list_checksum:', sums[0] * sums[1])




