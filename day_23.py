
from time import time
from mpl_toolkits.mplot3d import Axes3D  # need for the 3d projection
import matplotlib.pyplot as plt


class Nanobot:
    def __init__(self, desc):
        pos, radius = desc.split(', ')
        x, y, z = (int(tmp) for tmp in pos[5:-1].split(','))
        self.pos = (x, y, z)
        self.radius = int(radius[2:])


def distance(p1, p2):
    return sum(abs(a - b) for a, b in zip(p1, p2))


def update(best_in_range, best_point, bots, point):
    bots_in_range_of_point = len([tmp for tmp in bots if distance(tmp.pos, point) <= tmp.radius])
    if bots_in_range_of_point > best_in_range:
        #print(f'{bots_in_range_of_point} of {point} with distance {distance((0, 0, 0), point)}')
        best_in_range = bots_in_range_of_point
        best_point = point
    elif bots_in_range_of_point == best_in_range:
        if distance((0, 0, 0), point) < distance((0, 0, 0), best_point):
            #print(f'{bots_in_range_of_point} of {point} with distance {distance((0, 0, 0), point)}')
            best_in_range = bots_in_range_of_point
            best_point = point
    return best_in_range, best_point


def do_search(bots, point):
    prev_best_point = None
    best_point = point
    best_num = len([tmp for tmp in bots if distance(tmp.pos, best_point) <= tmp.radius])
    while prev_best_point != best_point:
        prev_best_point = best_point
        x, y, z = best_point
        for dx, dy, dz in [(1, 1, 1), (1, 1, 0), (1, 0, 1), (0, 1, 1),
                           (1, 0, 0), (0, 1, 0), (0, 0, 1)]:
            for f in range(20, -1, -1):
                point = (x+dx*2**f, y+dy*2**f, z+dz*2**f)
                best_num, best_point = update(best_num, best_point, bots, point)
                point = (x-dx*2**f, y-dy*2**f, z-dz*2**f)
                best_num, best_point = update(best_num, best_point, bots, point)
    return best_num, best_point


def main():
    bots = []
    for line in open('day_23.data'):
        bots.append(Nanobot(line))

    max_radius_bot = [x for x in sorted(bots, key=lambda tmp: -tmp.radius)][0]
    bots_in_range = len([tmp for tmp in bots if distance(tmp.pos, max_radius_bot.pos) <= max_radius_bot.radius])
    print(f'number of nanobots within range of the bot with greatest range is: {bots_in_range}')

    x = sum(bot.pos[0] for bot in bots) // len(bots)
    y = sum(bot.pos[1] for bot in bots) // len(bots)
    z = sum(bot.pos[2] for bot in bots) // len(bots)
    avg_point = (x, y, z)

    best_num = 0
    best_point = (0, 0, 0)
    for point in [(0, 0, 0), max_radius_bot.pos, avg_point]:
        best_num, best_point = update(best_num, best_point, bots, point)
    for bot in bots:
        point = bot.pos
        tmp_best_num, tmp_best_point = do_search(bots, point)
        if (tmp_best_num > best_num or
                (tmp_best_num == best_num and distance((0, 0, 0), tmp_best_point) < distance((0, 0, 0), best_point))):
            best_num = tmp_best_num
            best_point = tmp_best_point

            print(f'{best_num} of {best_point} with distance {distance((0, 0, 0), best_point)}')


if __name__ == '__main__':
    start = time()
    main()
    print(f'Time: {time()-start:.3}')
