
from time import time
from itertools import count

from world2d import World2d


class World(World2d):
    def __init__(self, input_data, active_waters):
        World2d.__init__(self, [[]])
        self.create_world(input_data)
        self.active_waters = active_waters

    def create_world(self, input_data):
        for line in input_data:
            a, b = line.replace('\n', '').split(', ')
            a_var, a_value = a.split('=')
            a_value = int(a_value)
            b_var, b_range = b.split('=')
            b_min, b_max = map(int, b_range.split('..'))

            for b_value in range(b_min, b_max +1):
                if a_var == 'x':
                    self.squares[(a_value, b_value)] = '#'
                else:
                    self.squares[(b_value, a_value)] = '#'

    def evolve_water(self):
        if len(self.active_waters) == 0:
            wx, wy = self.get_coord_of('w')
            Wx, Wy = self.get_coord_of('W')
            print(f'total waters: {len(wx) + len(Wx)}, still water: {len(Wx)}')
            raise StopIteration

        min_x, max_x, min_y, max_y = self.xy_limits
        num_active = len(self.active_waters)
        for index in range(num_active):
            x, y = self.active_waters[index]

            if y < max_y:
                if self.plot_y_limits is None:
                    plot_max_y = y
                else:
                    _, plot_max_y = self.plot_y_limits
                plot_max_y = max(plot_max_y, y + 15)
                self.plot_y_limits = plot_max_y - 220, plot_max_y

                pos = (x, y + 1)
                if pos not in self.squares:
                    if y+1 >= min_y:
                        self.squares[pos] = 'w'
                    self.active_waters.append(pos)
                elif self.squares[pos] != 'w':
                    side_spread = False
                    for dx in [-1, 1]:
                        pos = (x+dx, y)
                        if pos not in self.squares:
                            self.squares[pos] = 'w'
                            self.active_waters.append(pos)
                            side_spread = True
                    if not side_spread:
                        if self.is_water_enclosed(x, y):
                            self.make_water_static(x, y)
        self.active_waters = self.active_waters[num_active:]

    def is_water_enclosed(self, x, y):
        walls = 0
        for dx in [-1, 1]:
            for step in count():
                square = self.squares.get((x + dx * step, y), ' ')
                if square not in ['w', 'W']:
                    if square == '#':
                        walls += 1
                    break
        return walls == 2

    def make_water_static(self, x, y):
        for dx in [-1, 1]:
            for step in count():
                square = self.squares.get((x + dx * step, y), ' ')
                if square in ['w', 'W']:
                    self.squares[(x + dx * step, y)] = 'W'
                    pos_above = (x+dx*step, y - 1)
                    if self.squares.get(pos_above, ' ') == 'w':
                        if pos_above not in self.active_waters:
                            self.active_waters.append(pos_above)
                else:
                    break

    def animate(self, minute):
        self.evolve_water()
        return World2d.animate(self, minute)


def main(graphics):
    world = World(open('day_17.data'), [(500, 0)])

    if graphics:
        plot_objects = {'#': 'rs',
                        'w': 'co',
                        'W': 'bs'}
        world.run_animation('Advent-of-code 2018: Day 17', plot_objects, 40, inverted_y=True, control_y_limits=False)
    else:
        try:
            while True:
                world.evolve_water()
        except StopIteration:
            pass


if __name__ == '__main__':
    start = time()
    main(graphics=True)
    print(f'Time: {time()-start:.3}')
