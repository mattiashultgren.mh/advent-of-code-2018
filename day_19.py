
from time import time

from computer import Computer


def sum_of_divisor(n):
    s = 0
    for d in range(1, n+1):
        if n%d == 0:
            s += d
    return s


def part(n):
    computer = Computer(open('day_19.data'), [n-1, 0, 0, 0, 0, 0])
    for cnt, _ in enumerate(computer.execute_program()):
        if (cnt % 10000000) == 0 and cnt > 0:
            print(cnt, computer.state)
    print(f'Part {n}: {computer.state[0]} after {cnt} executed instructions')


def main():
    print(sum_of_divisor(1028))
    print(sum_of_divisor(10551428))
    part(1)
    part(2)


if __name__ == '__main__':
    start = time()
    main()
    print(f'Time: {time()-start:.3}')
