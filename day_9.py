
import blist


test_cases = [(9, 25, 32),
              (10, 1618, 8317),
              (13, 7999, 146373),
              (17, 1104, 2764),
              (21, 6111, 54718),
              (30, 5807, 37305),
              (405, 70953, 0),
              (405, 70953*100, 0)]


def place_marble(players, ring, player, marble, current):
    if marble % 23 == 0:
        new_current = (current - 7) % len(ring)
        players[player] += marble + ring[new_current]
        del ring[new_current]
        return new_current % len(ring)
    else:
        new_current = (current + 2) % len(ring)
        ring.insert(new_current, marble)
        return new_current


def play(num_players, max_marble):
    players = [0] * num_players
    ring = blist.blist([0])
    current = 0

    for marble in range(1, max_marble + 1):
        current = place_marble(players, ring, marble % num_players, marble, current)
    return max(players)


for players, max_marble, score in test_cases:
    s = play(players, max_marble)
    if s == score:
        print('passed test case')
    else:
        print(f'Playing with {players} players, {max_marble} last marble score should give {score} but we got {s}')
