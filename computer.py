

class Computer:
    opcodes = {
        'addr': lambda state, code: state[code[1]] + state[code[2]],
        'addi': lambda state, code: state[code[1]] + code[2],
        'mulr': lambda state, code: state[code[1]] * state[code[2]],
        'muli': lambda state, code: state[code[1]] * code[2],
        'banr': lambda state, code: state[code[1]] & state[code[2]],
        'bani': lambda state, code: state[code[1]] & code[2],
        'borr': lambda state, code: state[code[1]] | state[code[2]],
        'bori': lambda state, code: state[code[1]] | code[2],
        'setr': lambda state, code: state[code[1]],
        'seti': lambda state, code: code[1],
        'gtir': lambda state, code: 1 if code[1] > state[code[2]] else 0,
        'gtri': lambda state, code: 1 if state[code[1]] > code[2] else 0,
        'gtrr': lambda state, code: 1 if state[code[1]] > state[code[2]] else 0,
        'eqir': lambda state, code: 1 if code[1] == state[code[2]] else 0,
        'eqri': lambda state, code: 1 if state[code[1]] == code[2] else 0,
        'eqrr': lambda state, code: 1 if state[code[1]] == state[code[2]] else 0,
    }

    @staticmethod
    def execute(state, instruction):
        state[instruction[3]] = Computer.opcodes[instruction[0]](state, instruction)

    @staticmethod
    def parse_program(program):
        instructions = []
        ip_reg = None
        for line in program:
            line = line.replace('\n', '')
            if line.startswith('#ip '):
                ip_reg = int(line[-1])
            else:
                instruction, *parameters = line.split(' ')
                instructions.append(tuple([instruction, *map(int, parameters)]))
        return instructions, ip_reg

    def __init__(self, program, state=[0]*6):
        self.instructions, self.ip_reg = Computer.parse_program(program)
        self.ip = 0
        self.state = state

    def execute_program(self):
        while self.ip < len(self.instructions):
            if self.ip_reg is not None:
                self.state[self.ip_reg] = self.ip
            Computer.execute(self.state, self.instructions[self.ip])
            if self.ip_reg is not None:
                self.ip = self.state[self.ip_reg] + 1
            yield True
