

class Grid:
    def __init__(self, serial):
        self.grid = list()
        for y in range(1, 301):
            line = list()
            for x in range(1, 301):
                line.append(Grid.power_level(x, y, serial))
            self.grid.append(line)

    @staticmethod
    def power_level(x, y, serial):
        rack_id = x + 10
        power = ((rack_id * y) + serial) * rack_id
        return (power // 100) % 10 - 5

    def get_max_square_from_x(self, x, size):
        max_power = -5 * size**2
        max_y = 1
        seg_size = list()
        seg_sum = 0
        for y in range(1, 302 - size):
            seg_size.append(sum(self.grid[y-1][x-1:x-1+size]))
            seg_sum += seg_size[-1]
            if len(seg_size) == size:
                if seg_sum > max_power:
                    max_y, max_power = y-size+1, seg_sum
                seg_sum -= seg_size[0]
                del seg_size[0]
        return max_y, max_power

    def find_max_square_one_size(self, size):
        max_y, max_power = self.get_max_square_from_x(1, size)
        max_x = 1
        for x in range(2, 302 - size):
            tmp = self.get_max_square_from_x(x, size)
            if tmp[-1] > max_power:
                max_y, max_power = tmp
                max_x = x
        return max_x, max_y, max_power

    def find_max_square(self, sizes):
        max_x, max_y, max_power = self.find_max_square_one_size(sizes[0])
        max_size = sizes[0]
        for size in sizes[1:]:
            tmp = self.find_max_square_one_size(size)
            if tmp[-1] > max_power:
                max_x, max_y, max_power = tmp
                max_size = size
        return max_x, max_y, max_size, max_power


assert(Grid.power_level(3, 5, 8) == 4)
assert(Grid.power_level(122, 79, 57) == -5)
assert(Grid.power_level(217, 196, 39) == 0)
assert(Grid.power_level(101, 153, 71) == 4)

assert(Grid(18).find_max_square([3]))
assert(Grid(18).find_max_square([3]) == (33, 45, 3, 29))
assert(Grid(42).find_max_square([3]) == (21, 61, 3, 30))
assert(Grid(18).find_max_square([16]) == (90, 269, 16, 113))
assert(Grid(42).find_max_square([12]) == (232, 251, 12, 119))

print('part1', Grid(6303).find_max_square([3]))
print('part2', Grid(6303).find_max_square(range(1, 301)))
