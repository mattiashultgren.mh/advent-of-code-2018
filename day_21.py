
from time import time

from computer import Computer


def integer_underflow():
    a = 2**24
    b, c, d, e, f = 0, 0, 0, 0, 0

    halting_numbers = set()

    while True:

        e = b | 0x10000
        b = 16298264

        while True:
            b = ((b + (e % (2**8))) * 65899) % (2**24)
            if e < 256:
                break
            e //= 256

        if b in halting_numbers:
            break
        last = b
        if len(halting_numbers) == 0:
            first = b
        halting_numbers.add(b)
        #print(len(halting_numbers), b)
        if b == a:
            break

    print(f'First halting number: {first}')
    print(f'Last halting number: {last}')


def test_with(n):
    computer = Computer(open('day_21.data'), [n, 0, 0, 0, 0, 0])
    halting_numbers = set()
    for cnt, _ in enumerate(computer.execute_program()):
        if computer.ip == 30:
            b = computer.state[1]
            if b in halting_numbers:
                break
            halting_numbers.add(b)
            print(cnt, len(halting_numbers), b)

    print(f'Started with A={n}: {computer.state[0]} after {cnt} executed instructions')


def main():
    integer_underflow()
    #test_with(3345459)

    #test_with(2**24)


if __name__ == '__main__':
    start = time()
    main()
    print(f'Time: {time()-start:.3}')
