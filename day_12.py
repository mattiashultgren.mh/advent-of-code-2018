
initial_state = '.#..##..#.....######.....#....####.##.#.#...#...##.#...###..####.##.##.####..######......#..##.##.##'
trans_pattern = '''.##.# => #
###.# => #
...#. => #
#...# => #
.#... => #
.#.## => #
#..#. => #
##.#. => #
.#### => #
#.##. => #
.#..# => #
..#.# => #
##..# => #
.#.#. => #
#.#.# => #'''


initial_states = '#..#.#..##......###...###'
trans_patterns = '''...## => #
..#.. => #
.#... => #
.#.#. => #
.#.## => #
.##.. => #
.#### => #
#.#.# => #
#.### => #
##.#. => #
##.## => #
###.. => #
###.# => #
####. => #'''


def parse_state(s):
    return int(''.join(reversed(s.replace('#', '1').replace('.', '0'))), 2)


def parse_transitions(s):
    patterns = set()
    for line in s.split('\n'):
        if line[-1] == '#':
            patterns.add(parse_state(line.split(' ')[0]))
    return patterns


def state_to_string(state):
    return ((''.join(reversed(bin(state)[2:]))).replace('0', '.')).replace('1', '#')


def evolve(state, patterns):
    new_state = 0
    b = 1
    while b <= state:
        cur = ((31 * b) & state) // b
        if cur in patterns:
            new_state += 4*b
        b *= 2
    return new_state


def get_state_sum(state, n):
    state_sum = 0
    b = 1
    while b <= state:
        if b & state != 0:
            state_sum += n
#            print(n, state_sum)
        b *= 2
        n += 1
    return state_sum

patterns = parse_transitions(trans_pattern)
state = parse_state(initial_state)
prev_state = state
shifts = 0
limit = 50000000000

print(''.join([' ']*10), state_to_string(state))
for round in range(20):
    while 31 & state != 0:
        state *= 2
        shifts += 1
    state = evolve(state, patterns)
    print(''.join([' ']*(10-shifts)), state_to_string(state))

print('part1', get_state_sum(state, -shifts))

prev_state = state
for round in range(20, limit):
    state = evolve(state, patterns)
    while 31 & state != 0:
        state *= 2
        shifts += 1
    while 63 & state == 0:
        state //= 2
        shifts -= 1
    print(get_state_sum(state, -shifts), state_to_string(state))
    if state == prev_state:
        break
    prev_state = state

shifts -= (limit - round - 1)
print('part2', get_state_sum(state, -shifts), shifts)
