
import collections


Position = collections.namedtuple('Position', ['x', 'y'])


class Area:
    def __init__(self, pos):
        self.positions = set()
        self.start_position = pos
        self.prev_size = 0
        self.grows = True
        self.infinite = False

    def round_finished(self):
        if self.prev_size == len(self.positions):
            self.grows = False
        self.prev_size = len(self.positions)


def update_from_current(ar, cur, occ):
    for p, a in cur.items():
        ar[a].positions.add(p)
        occ.add(p)


def grow(pos, cur, occ, top, bottom):
    for p, a in pos.items():
        for dx, dy in [(-1, 0), (0, -1), (1, 0), (0, 1)]:
            p2 = Position(p.x + dx, p.y + dy)
            if p2 not in occ:
                if p2.x < bottom.x or p2.x > top.x or p2.y < bottom.y or p2.y > top.y:
                    areas[a].infinite = True
                elif p2 in cur:
                    if a != cur[p2]:
                        del cur[p2]
                        occ.add(p2)
                else:
                    cur[p2] = a


def distance(p, p2):
    return abs(p.x - p2.x) + abs(p.y - p2.y)


areas = list()
occupied = set()
current = dict()
prev = None
border_top, border_bottom = None, None

with open('day_6.data', 'r') as file:
    for line in file:
        p = Position(*(int(a) for a in line.split(',')))
        areas.append(Area(p))
        current[p] = len(areas) - 1
        if border_top is None:
            border_top = p
        if border_bottom is None:
            border_bottom = p
        border_top = Position(max(p.x, border_top.x), max(p.y, border_top.y))
        border_bottom = Position(min(p.x, border_bottom.x), min(p.y, border_bottom.y))

while sum(1 for a in areas if a.grows) > 0:
    update_from_current(areas, current, occupied)
    prev, current = current, dict()
    grow(prev, current, occupied, border_top, border_bottom)
    for ar in areas:
        ar.round_finished()

limit = 10000
x_avg = sum(ar.start_position.x for ar in areas) // len(areas)
y_avg = sum(ar.start_position.y for ar in areas) // len(areas)
region = set()
region_outer = {(Position(x_avg, y_avg), limit)}
while len(region_outer) > 0:
    prev_outer, region_outer = region_outer, set()
    for p, dist in prev_outer:
        for dx, dy in [(-1, 0), (0, -1), (1, 0), (0, 1)]:
            pn = Position(p.x + dx, p.y + dy)
            if pn not in region:
                if dist + len(areas) < limit:
                    total_distance = dist + len(areas)
                else:
                    total_distance = sum(distance(pn, ar.start_position) for ar in areas)
                if total_distance < limit:
                    region.add(pn)
                    region_outer.add((pn, total_distance))
print('part1', max(len(ar.positions) for ar in areas if not ar.infinite))
print('part2', len(region))
