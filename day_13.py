
from enum import IntEnum


class Direction(IntEnum):
    Left = 0
    Up = 1
    Right = 2
    Down = 3


class Option(IntEnum):
    Left = 0
    Straight = 1
    Right = 2


class Cart:
    def __init__(self, x, y, direction):
        self.x = x
        self.y = y
        self.dir = direction
        self.next_turn = Option.Left

    def __repr__(self):
        return f'Cart({self.x}, {self.y}, {self.dir}, {self.next_turn})'

    def __lt__(self, other):
        if self.y < other.y:
            return True
        if self.y == other.y and self.x < other.x:
            return True
        return False

    def move(self, track):
        deltas = [(-1, 0), (0, -1), (1, 0), (0, 1)]
        new_pos = (self.x + deltas[self.dir][0], self.y + deltas[self.dir][1])
        tmp = track[new_pos[1]][new_pos[0]]
        if tmp == '\\':
            cart.dir = Direction(cart.dir ^ 1)
        elif tmp == '/':
            cart.dir = Direction((cart.dir -1 + 2*(cart.dir &1)) % 4)
        elif tmp == '+':
            if cart.next_turn == Option.Left:
                cart.dir = Direction((cart.dir-1) % 4)
            elif cart.next_turn == Option.Right:
                cart.dir = Direction((cart.dir+1) % 4)
            cart.next_turn = Option((cart.next_turn + 1) % 3)
        self.x, self.y = new_pos


cart_chars = ('<', '^', '>', 'v')
carts = set()
positions = set()

track = [list(line.replace('\n', '')) for line in open('day_13.data', 'r')]

[print(''.join(row)) for row in track]
print()

for y, row in enumerate(track):
    for x, c in enumerate(row):
        if c in cart_chars:
            direction = Direction(cart_chars.index(c))
            carts.add(Cart(x, y, direction))
            positions.add((x, y))

for cart in carts:
    if cart.dir == Direction.Left or cart.dir == Direction.Right:
        track[cart.y][cart.x] = '-'
    else:
        track[cart.y][cart.x] = '|'

[print(''.join(row)) for row in track]
print()

for cart in sorted(carts):
    print(cart)
print(positions)
print()


while len(carts) > 1:
    crashes = set()
    for cart in sorted(carts):
        po = (cart.x, cart.y)
        positions.remove(po)
        if po not in crashes:
            cart.move(track)
            p = (cart.x, cart.y)
            if p in positions:
                crashes.add(p)
                print(f'crash at {p}')
        positions.add(p)
    for cart in sorted(carts):
        p = (cart.x, cart.y)
        if p in crashes:
            carts.remove(cart)
            if p in positions:
                positions.remove(p)


for cart in sorted(carts):
    print(cart)
