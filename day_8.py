

class Node:
    def __init__(self):
        self.children = list()
        self.metas = list()

    def sum_metas(self):
        return sum(self.metas) + sum(c.sum_metas() for c in self.children)

    def get_value(self):
        if len(self.children) == 0:
            return sum(self.metas)
        return sum(self.children[m-1].get_value() for m in self.metas if m - 1 < len(self.children))


def parse(n):
    node = Node()
    num_metas, num_children = n[-2:]
    del n[-2:]

    for _ in range(num_children):
        node.children.append(parse(n))

    node.metas = n[-num_metas:]
    del n[-num_metas:]
    return node

# getting the data in the reversed order to make the del's faster as they all now is in the end
nums = [int(x) for x in reversed(open('day_8.data', 'r').read().split(' '))]
root = parse(nums)
print('part1', root.sum_metas())
print('part2', root.get_value())
