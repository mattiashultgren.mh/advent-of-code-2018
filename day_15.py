
import collections
from time import time
from itertools import count


Position = collections.namedtuple('Position', ['x', 'y'])


class Critter:
    def __init__(self, name, x, y):
        self.name = name
        self.x = x
        self.y = y
        self.hp = 200
        self.dmg = 3

    def __repr__(self):
        return f'{self.name}({self.hp}, {self.x}, {self.y})'

    def __lt__(self, other):
        if self.y < other.y:
            return True
        if self.y == other.y and self.x < other.x:
            return True
        return False

    def attack(self, critters, attackable):
        target = None
        for x, y in [(0, -1), (-1, 0), (1, 0), (0, 1)]:
            p = Position(self.x + x, self.y + y)
            if p in attackable:
                for critter in critters:
                    if critter.x == p.x and critter.y == p.y:
                        if target is None or critter.hp < target.hp:
                            target = critter
        target.hp -= self.dmg
        if target.hp <= 0:
            critters.remove(target)

    def move(self, possible_steps):
        for x, y in [(0, -1), (-1, 0), (1, 0), (0, 1)]:
            p = Position(self.x + x, self.y + y)
            if p in possible_steps:
                self.x, self.y = p
                return


def get_targets(critters, name):
    return set(c for c in critters if c.name != name)


def get_adjacent(world, forbidden, positions):
    dest = set()
    for p in positions:
        for x, y in [(1, 0), (-1, 0), (0, 1), (0, -1)]:
            np = Position(p.x + x, p.y + y)
            if np not in forbidden and world[np.y][np.x] != '#':
                dest.add(np)
    return dest


def get_nearest(world, forbidden, nearby, targets):
    next_iter = nearby
    dont_use = forbidden.union(next_iter)
    while len(next_iter) > 0 and len(next_iter.intersection(targets)) == 0:
        next_iter = get_adjacent(world, dont_use, next_iter)
        dont_use |= next_iter
    return next_iter.intersection(targets)


def show_world(world, critters, special=None):
    for row in world:
        for x in range(len(row)):
            if row[x] in ['G', 'E', 'g', 'e']:
                row[x] = '.'
    for critter in critters:
        if critter is special:
            world[critter.y][critter.x] = critter.name.lower()
        else:
            world[critter.y][critter.x] = critter.name

    for row in world:
        print(''.join(row))
    print(sorted(critters))
    print()


def simulate(world, critters):
    for rounds in count():
        for critter in sorted(critters):
            if critter.hp <= 0:
                continue
            targets = set(Position(t.x, t.y) for t in get_targets(critters, critter.name))
            if len(targets) == 0:
                return rounds

            nearby = get_adjacent(world, set(), {Position(critter.x, critter.y)})
            attackable = nearby.intersection(targets)
            if len(attackable) == 0:
                forbidden = set(Position(t.x, t.y) for t in critters)
                target_squares = get_adjacent(world, forbidden, targets)
                nearest_targets = get_nearest(world, forbidden, nearby - forbidden, target_squares)
                if len(nearest_targets) > 0:
                    nearest_targets = {sorted(nearest_targets, key=lambda a: a.y*10000+a.x)[0]}
                    possible_steps = get_nearest(world, forbidden, nearest_targets, nearby)
                    critter.move(possible_steps)
                    nearby = get_adjacent(world, set(), {Position(critter.x, critter.y)})
                    attackable = nearby.intersection(targets)
            if len(attackable) > 0:
                critter.attack(critters, attackable)


def load_world():
    world = [list(line.replace('\n', '')) for line in open('day_15.data', 'r')]
    critters = set()

    for y, row in enumerate(world):
        for x, p in enumerate(row):
            if p in ['G', 'E']:
                critters.add(Critter(p, x, y))
                world[y][x] = '.'
    return world, critters


def main():
    world, critters = load_world()
    elfs_at_start = sum(1 for critter in critters if critter.name == 'E')

    rounds = simulate(world, critters)
    hps = sum(critter.hp for critter in critters)
    part1_score = rounds * hps

    attack_power_min = 3
    attack_power_high = None
    while attack_power_min+1 != attack_power_high:
        if attack_power_high is None:
            attack_power = attack_power_min * 2
        else:
            attack_power = (attack_power_min + 1 + attack_power_high) // 2
        world, critters = load_world()

        for critter in critters:
            if critter.name == 'E':
                critter.dmg = attack_power

        rounds = simulate(world, critters)
        elfs_at_end = sum(1 for critter in critters if critter.name == 'E')

        hps = sum(critter.hp for critter in critters)
        if elfs_at_start == elfs_at_end:
            part2_score = rounds * hps
            part2_attack_power = attack_power
            attack_power_high = attack_power
        else:
            attack_power_min = attack_power

    print(f'Part1: {part1_score}')
    print(f'Part2: {part2_score} with elf attack power {part2_attack_power}')


if __name__ == '__main__':
    start = time()
    main()
    print(f'Time: {time() - start:.3}')

