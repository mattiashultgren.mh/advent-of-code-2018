
import time


class Point:
    def __init__(self, px, py, vx, vy):
        self.px = px
        self.py = py
        self.vx = vx
        self.vy = vy

    def distance_fake(self, po):
        return max(abs(self.px - po.px), abs(self.py - po.py))

    def move(self):
        self.px += self.vx
        self.py += self.vy


def any_singular_point(points):
    for p in points:
        any_close_found = False
        for p2 in points:
            if p != p2 and p2:
                if p.distance_fake(p2) <= 1:
                    any_close_found = True
        if not any_close_found:
            return True
    return False


def show(points):
    min_x, max_x = 1000, -1000
    min_y, max_y = 1000, -1000
    ps = set()
    for p in points:
        min_x = min(min_x, p.px)
        max_x = max(max_x, p.px)
        min_y = min(min_y, p.py)
        max_y = max(max_y, p.py)
        ps.add((p.px, p.py))
    for y in range(min_y, max_y + 1):
        s = ''
        for x in range(min_x, max_x + 1):
            if (x, y) in ps:
                s += '#'
            else:
                s += ' '
        print(s)
    print()


def main():
    points = list()
    with open('day_10.data', 'r') as file:
        for line in file:
            p = Point(*(int(x) for x in line.replace('>\n', '').replace('>', ',')
                      .replace('position=<', '').replace('velocity=<', '')
                      .split(',')))
            points.append(p)

    t = 0
    while any_singular_point(points):
        t += 1
        for p in points:
            p.move()
    print(f't={t}')
    show(points)


if __name__ == '__main__':
    start = time.time()
    main()
    print(time.time() - start)
