
import collections


Claim = collections.namedtuple('Claim', ['id', 'x', 'y', 'dx', 'dy'])


def parse_claim(s):
    ts = s.replace(' ', '').replace('#', '').replace('\n', '')
    for p in ['@', 'x', ':']:
        ts = ts.replace(p, ',')
    return Claim(*(int(x) for x in ts.split(',')))


def add_claim(dd, claim, inc):
    m = 0
    for x in range(claim.x, claim.x + claim.dx):
        for y in range(claim.y, claim.y + claim.dy):
            dd[(x, y)] += inc
            m = max(m, dd[(x, y)])
    return m


overlaps = 0
claims = collections.defaultdict(int)
pot_non_overlapping = set()
non_overlapping = set()

with open('day_3.data', 'r') as file:
    for line in file:
        claim = parse_claim(line)
        if add_claim(claims, claim, 1) == 1:
            pot_non_overlapping.add(claim)

overlaps = sum(min(x, 2) - 1 for x in claims.values())

for claim in pot_non_overlapping:
    if add_claim(claims, claim, 0) == 1:
        non_overlapping.add(claim)

print('overlaps', overlaps)
print('non_overlapping', non_overlapping)
