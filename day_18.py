
from time import time

from world2d import World2d


class World(World2d):
    def __init__(self, iter_2d, ignore_squares):
        World2d.__init__(self, iter_2d, ignore_squares)

        self.dup_1_minute = None
        self.dup_2_minute = None
        self.dup_3_minute = None
        self.states = dict()

    def evolve(self):
        squares = dict()

        for x, y in self.coord_within_limits:
            square = self.squares.get((x, y), self.default_square)
            if square == '.':
                if 3 <= sum(1 for tmp in self.adj_squares(x, y) if tmp == '|'):
                    square = '|'
            elif square == '|':
                if 3 <= sum(1 for tmp in self.adj_squares(x, y) if tmp == '#'):
                    square = '#'
            elif square == '#':
                if (0 == sum(1 for tmp in self.adj_squares(x, y) if tmp == '#') or
                        0 == sum(1 for tmp in self.adj_squares(x, y) if tmp == '|')):
                    square = '.'
            squares[(x, y)] = square

        self.squares = squares

    def resource_value(self):
        wooded = sum(1 for tmp in self.squares.values() if tmp == '|')
        lumberyards = sum(1 for tmp in self.squares.values() if tmp == '#')
        return wooded * lumberyards

    def animate(self, minute):
        if minute == 10:
            print(f'resource value after {minute} minutes {self.resource_value()}')
        ws = self.get_string()
        if ws in self.states:
            if self.dup_1_minute is None:
                self.dup_1_minute = minute
            elif self.states[ws] == 2:
                if self.dup_2_minute is None:
                    self.dup_2_minute = minute
                diff = self.dup_2_minute - self.dup_1_minute
                goal_minute = self.dup_2_minute + (10**9 - self.dup_1_minute) % diff
                if minute == goal_minute:
                    print(f'estimated resource value after {10**9} minutes {self.resource_value()}')
            elif self.states[ws] == 3:
                self.dup_3_minute = minute
                self.states = dict()

        if self.dup_3_minute is None:
            self.states[ws] = 1 + self.states.get(ws, 0)
        self.evolve()

        return World2d.animate(self, minute)


def main():
    world = World(open('day_18.data'), ['\n'])

    plot_objects = {'|': 'o',
                    '#': 's'}
    world.run_animation('Advent-of-code 2018: Day 18', plot_objects, 100)


if __name__ == '__main__':
    start = time()
    main()
    print(f'Time: {time()-start:.3}')
